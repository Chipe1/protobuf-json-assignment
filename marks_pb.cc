#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<time.h>
#include <iomanip>
#include "marks.pb.h"

using namespace std;

typedef struct __st_CourseMarks{
    string name;
    int marks;
}myCourseMarks;

typedef struct __st_Student{
    string name;
    int rollno;
    vector<myCourseMarks> marks;
}myStudent;

typedef struct __st_Result{
    vector<myStudent> student;
}myResult;

myStudent process_line(ifstream &);
vector<string> split_str(string, char);
void write_json(string, string);
void write_protobuf(string, string);
void read_protobuf(string, string);
void read_json(string, string);
string read_stack(ifstream &);
string remove_useless(string, string);

double tot_time;
long long tot_data;
    
int main(int argc, char *argv[]){
    GOOGLE_PROTOBUF_VERIFY_VERSION;
    if(argc != 3){
	cerr << "Must provide 3 arguments" << endl;
	return -1;
    }

    string type(argv[1]), fname(argv[2]);
    // cout << "type: " << type << " name: " << fname << endl;
    // return 0;
    if(type == "sj"){
	write_json(fname, "result.json");
    }
    else if(type == "sp"){
	write_protobuf(fname, "result_protobuf");
    }
    else if(type == "dj"){
	read_json(fname, "output_json.txt");
    }
    else if(type == "dp"){
	read_protobuf(fname, "output_protobuf.txt");
    }
    else if(type == "tj"){
	time_t t_s, t_e;
	time(&t_s);
	write_json(fname, "result.json");
	time(&t_e);
	cout << "Serialization time: " << setprecision(20) << difftime(t_e, t_s) << endl;

	time(&t_s);
	read_json("result.json", "output_json.txt");
	time(&t_e);
	cout << "Deserialization time: " << setprecision(20) << difftime(t_e, t_s) << endl;

	ifstream mid_file("result.json", ios::ate | ios::binary);
	cout << "Conversion size: " << mid_file.tellg() << endl;
    }
    else if(type == "tp"){
	time_t t_s, t_e;
	time(&t_s);
	write_protobuf(fname, "result_protobuf");
	time(&t_e);
	cout << "Serialization time: " << setprecision(20) << difftime(t_e, t_s) << endl;

	time(&t_s);
	read_protobuf("result_protobuf", "output_protobuf.txt");
	time(&t_e);
	cout << "Deserialization time: " << setprecision(20) << difftime(t_e, t_s) << endl;

	ifstream mid_file("result_protobuf", ios::ate | ios::binary);
	cout << "Conversion size: " << mid_file.tellg() << endl;
    }
    else{
	cerr << "Unrecognized command" << endl;
    }
    
    // write_protobuf("input_sample", "proto_out");
    // write_json("input_sample", "json_out.json");
    // read_protobuf("proto_out", "re_proto.txt");
    // read_json("json_out.json", "re_json.txt");
    
    return 0;
}
    
myStudent process_line(ifstream &fin){
    myStudent toret;
    string s;
    vector<string> ss;
    fin >> s;
    if(s == ""){
	toret.name = "";
	return toret;
    }
    ss = split_str(s, ':');
    toret.name = split_str(ss[0], ',')[0];
    toret.rollno = stoi(split_str(ss[0], ',')[1]);
    for(int i = 1; i < ss.size(); i++){
	myCourseMarks m;
	m.name = split_str(ss[i], ',')[0];
	m.marks = stoi(split_str(ss[i], ',')[1]);
	toret.marks.push_back(m);
    }
    return toret;
}

vector<string> split_str(string s, char delim){
    vector<string> toret;
    // Turn the string into a stream.
    stringstream ss(s);
    string tok;
    
    while(getline(ss, tok, delim)){
	toret.push_back(tok);
    }
    
    return toret;
}

void write_json(string infile, string ofile){

    ifstream fin;
    ofstream fout;

    fin.open(infile);
    
    fout.open(ofile, ios::trunc);
    fout << '[';
    
    int fst_line = 1;
    while(142857){
	// Read line
	myStudent stu;
	stu = process_line(fin);
	if(stu.name == "")
	    break;

	// Write line
	if(fst_line)
	    fst_line = 0;
	else
	    fout << ',';
	fout << '{';
	fout << "\"Name\":\"" << stu.name << "\",";
	fout << "\"CourseMarks\":[";
	vector<myCourseMarks> myc;
	myc = stu.marks;
	for(int j = 0; j < myc.size(); j++){
	    fout << '{';
	    fout << "\"CourseScore\":" << myc[j].marks << ",\"CourseName\":\"" << myc[j].name << "\"";
	    fout << '}';	    
	    if(j != myc.size() - 1)
		fout << ',';
	}
	fout << "],";
	fout << "\"RollNo\":" << stu.rollno;
	fout << '}';
    }
    fout << ']';
}

void write_protobuf(string infile, string ofile){

    ifstream fin;
    ofstream fout;
    Result result;

    fin.open(infile);
    
    fout.open(ofile, ios::binary | ios::trunc);
    while(142857){
	// Read line
	Student *stu;
	myStudent st;
	st = process_line(fin);
	if(st.name == "")
	    break;

	stu = result.add_student();
	stu->set_name(st.name);
	stu->set_rollnum(st.rollno);

	for(int i = 0; i < st.marks.size(); i++){
	    CourseMarks *m;
	    myCourseMarks marks;
	    marks = st.marks[i];
	    m = (*stu).add_marks();
	    m->set_name(marks.name);
	    m->set_score(marks.marks);
	}
    }
    if(!result.SerializeToOstream(&fout)){
    	cerr << "Failed to write report " << ofile << endl;
    	exit(1);
    }
}

void read_protobuf(string infile, string ofile){
    ifstream fin;
    ofstream fout;

    Result result;
    fin.open(infile, ios::binary);
    fout.open(ofile, ios::trunc);
    
    if(!result.ParseFromIstream(&fin)){
    	cerr << "Failed to read protobuf file " << infile << endl;
    	exit(1);
    }

    for(int i=0; i < result.student_size(); i++){
	Student stu;
	stu = result.student(i);
	fout << stu.name() << ',' << stu.rollnum();
	for(int j = 0; j < stu.marks_size(); j++){
	    CourseMarks marks;
	    marks = stu.marks(j);
	    fout << ':' << marks.name() << ',' << marks.score();
	}
	fout << endl;
    }
}

void read_json(string infile, string ofile){
    ifstream fin;
    ofstream fout;
    fin.open(infile);
    fout.open(ofile, ios::trunc);

    while(142857){
	fin.seekg(1, fin.cur);
	string s;
	vector<string> ss;
	s = read_stack(fin);
	if(s == "")
	    break;

	ss = split_str(s, ',');
	fout << remove_useless(split_str(ss[0], ':')[1], "\"") << ',';
	fout << split_str(ss[ss.size()-1], ':')[1];
	s = "";
	for(int i = 1; i < ss.size()-1; i++){
	    s += ":"+ss[i];
	}
	ss = split_str(s, ':');
	for(int i = 2; i < ss.size(); i += 4){
	    fout << ':' << remove_useless(ss[i+3], "{}[]\"") << ',' << remove_useless(ss[i+1], "{}[]\"");
	}
	fout << endl;
    }
}

string read_stack(ifstream &fin){
    string toret;
    char c, stchar, endchar;
    int stack;

    toret = "";
    stack = 1;
    fin >> c;
    if(c == '{'){
	stchar = '{';
	endchar = '}';
    }
    else if(c == '['){
	stchar = '[';
	endchar = ']';
    }
    else{
	return toret;
    }

    while(stack){
	fin >> c;
	toret += c;
	if(c == stchar)
	    stack++;
	else if(c == endchar)
	    stack --;
    }
    toret.pop_back();
}

string remove_useless(string s, string useless){
    string toret;
    int fl[256];

    for(int i = 0; i < 256; i++)
	fl[i] = 1;
    for(int i = 0; i < useless.size(); i++)
	fl[useless[i]] = 0;
    
    toret = "";
    for(int i = 0; i < s.size(); i++){
	if(fl[s[i]])
	    toret += s[i];
    }
    
    return toret;
}
