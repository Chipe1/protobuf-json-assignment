#!/bin/bash

case $1 in
    -c)
	make
	;;
    -s)
	case $2 in
	    -j)
		./a.out sj $3
		;;
	    -p)
		./a.out sp $3
		;;
	    *)
		echo "Unknown operation"
		;;
	esac
	;;
    -d)
	case $2 in
	    -j)
		./a.out dj $3
		;;
	    -p)
		./a.out dp $3
		;;
	    *)
		echo "Unknown operation"
		;;
	esac
	;;
    -t)
	case $2 in
	    -j)
		./a.out tj $3
		;;
	    -p)
		./a.out tp $3
		;;
	    *)
		echo "Unknown operation"
		;;
	esac
	;;
    *)
	echo "Unknown operation"
	;;
esac
